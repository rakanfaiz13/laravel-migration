@extends('extends.master')

@section('cast', 'active')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Cast Page</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active"><a href="/dashboard">Cast</a></li>
                        <li class="breadcrumb-item">Cast Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Cast Detail</h3>
            </div>
            <div class="card-body">
                <h3>Detail Information About Cast</h3> <br>
                <h5><strong>Name &nbsp;: </strong> {{ $cast->name }}</h5>
                <h5><strong>Age &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong> {{ $cast->umur }}</h5>
                <hr>
                <p>
                    as {{ $cast->bio }}
                </p>  
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
