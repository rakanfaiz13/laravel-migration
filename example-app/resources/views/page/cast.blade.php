@extends('extends.master')

@section('cast', 'active')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Cast Page</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Cast</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @if (session('msg'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('msg') }}! </strong>You should check in on some of those fields below.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">Cast Table</h3>
                    <a href="/cast/create"><button type="button" class="btn btn-primary btn-sm"><i
                                class="fas fa-plus"></i> &nbsp; Add Cast</button></a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Bio</th>
                            <th style="width: 150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i = 1
                        @endphp

                        @foreach($casts as $cast)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $cast->name }}</td>
                            <td>{{ $cast->umur }}</td>
                            <td>{{ $cast->bio }}</td>
                            <td>
                                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                    <div class="btn-group mr-2" role="group" aria-label="First group">
                                        <a href="/cast/{{ $cast->id }}"><button type="button"
                                                class="btn btn-primary btn-sm">
                                                <i class="fas fa-eye"></i></button></a>
                                    </div>

                                    <div class="btn-group mr-2" role="group" aria-label="First group">
                                        <a href="/cast/{{ $cast->id }}/edit"><button type="button"
                                                class="btn btn-secondary btn-sm">
                                                <i class="fas fa-cog"></i></button></a>
                                    </div>

                                    <form action="/cast/{{$cast->id}}" method="post">
                                        @csrf
                                        <div class="btn-group" role="group" aria-label="Second group">
                                            <button type="submit" class="btn btn-danger btn-sm"><i
                                                    class="far fa-trash-alt"></i></button>
                                            <input type="hidden" name="_method" value="DELETE">
                                        </div>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
