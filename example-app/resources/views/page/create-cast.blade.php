@extends('extends.master')

@section('cast', 'active')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create Cast Page</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                        <li class="breadcrumb-item active"><a href="/cast">Cast</a></li>
                        <li class="breadcrumb-item">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Input Cast</h3>
            </div>
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="/cast" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" name="name" class="form-control" id="inputName" placeholder="Enter Name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="inputAge">Age</label>
                        <input type="number" name="umur" id="inputAge" class="form-control" placeholder="Enter Age" value="{{ old('umur') }}">
                    </div>
                    <div class="form-group">
                        <label for="inputAge">Bio</label>
                        <textarea name="bio" id="" cols="30" rows="10" class="form-control" placeholder="Enter Bio..">{{ old('bio') }}</textarea>
                    </div>

                    <button class="btn btn-success" type="submit" name="submit">Create</button>
                </form>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
